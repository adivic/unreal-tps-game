# Singleplayer & Multiplayer TPS Game
Thank you for checking the game. This is a presentation of various stages (things)
in the development process. In this game is shown:
* ### Basic AI behavior tree 
    * Allows AI to follow patrol path and if sees a player starts shooting at him. 
    * Strafe around player
    * When lost his sight of a player, he starts searching hor him if that turns out to be a failure it is going back to his path.
* ### Inventory System
    * Allows a player to collect various attachments for his weapon
    * Collect health and armor
* ### Weapons camo to give a more efficient look.
    * Different weapons have different camos'
    * Modern and SF camos'
* ### Respawn system (Singleplayer & Multiplayer), character manager, dying logic
    * If AI dies he will respawn on his spawn point
    * In multiplayer when a player dies he respawns at spawn location.
* ### Animations
    * Character animations, Retargeting animations imported from internet.
    * Vaulting logic
    * Weapon animations.
* ### Multiplayer (Steam ***is Medentory***)
    * Implementing Lobby logic, connection to a lobby
    * Lobby Map, while waiting for a match to start
    * Lobby Chat
    * Player selection - Pick system
    * Kick Player out of a lobby 
    * Saving player thumbnail and player name
    * Setup map and match settings
    * Players leaderboard
    * Respawning after being killed

<br />

## In-Game futages
* #### Demo Map
     * ![Stage_1_a](/uploads/c93da6608b66adaae147aa55c90a61e3/Stage_1_a.PNG) ![Stage_1_b](/uploads/4b48fe9bbd7e5dbc7d2e1c83f843673d/Stage_1_b.PNG) ![Stage_1_d](/uploads/9cdf6442b2cd1d067d36add03791dbdc/Stage_1_d.PNG)
     * ![Stage_1_c](/uploads/e3c9046f7c271b4f2d2a34c528286a16/Stage_1_c.PNG) ![Stage_1_e](/uploads/51c177c5e85798289d4e3e214d1058fd/Stage_1_e.PNG)
* #### Killhouse
    * ![Killhouse_3](/uploads/d3513ff5f693e3849595b3ce3dfd2fb0/Killhouse_3.PNG) ![Killhouse_4](/uploads/6b57003d8b65725abe3944efc94dff0e/Killhouse_4.PNG)
    * ![Killhouse_1](/uploads/c0085be43b4c432baf4f2d95750b9b00/Killhouse_1.PNG) ![Killhouse_2](/uploads/a4f000444bb550854871b39aaf0a66ef/Killhouse_2.PNG)
* #### Forest
    * ![Forest_1](/uploads/099e87ea2bb0ba122476df1253aa7745/Forest_1.PNG) ![Forest_2](/uploads/fe231acc3af3ba27eb3de7ecf3984536/Forest_2.PNG) ![Forest_5](/uploads/92c8e0282bda858c40e44f89a1600e7c/Forest_5.PNG)
    * ![Forest_3](/uploads/cbc506d5e315f672fdf3543299d958d1/Forest_3.PNG) ![Forest_4](/uploads/0472e6a8fa4c15ff03e9eb4299a5efa5/Forest_4.PNG) 

