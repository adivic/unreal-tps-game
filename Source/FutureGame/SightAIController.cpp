// Fill out your copyright notice in the Description page of Project Settings.


#include "SightAIController.h"

ASightAIController::ASightAIController() {
	SetGenericTeamId(FGenericTeamId(5));
}

ETeamAttitude::Type ASightAIController::GetTeamAttitudeTowards(const AActor& Other) const {

	if (const APawn* OtherPawn = Cast<APawn>(&Other)) {

		if (const IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController())) {
			return ETeamAttitude::Neutral;
		} else {
			return ETeamAttitude::Hostile;
		}
	}
	return ETeamAttitude::Neutral;
}